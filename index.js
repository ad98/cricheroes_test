// all the varibles/reference used in code or in req.body might have 'n' = number 's' = string 'o' = object prepended to it, which refers appropriate data type.

const express = require("express");
const app = express();

// this is used to parse body data

app.use(express.json({ limit: "10kb" }));

// this is the main data chart point which is used as it is in every api call

const mainData = [
  {
    nRank: 1,
    sTeam: "Chennai Super Kings",
    nMatches: 7,
    nWon: 5,
    nLoss: 2,
    NRR: 0.771,
    oFor: {
      nRuns: 1130,
      nOvers: 133.167,
    },
    oAgainst: {
      nRuns: 1071,
      nOvers: 138.5,
    },
    nPoints: 10,
  },
  {
    nRank: 2,
    sTeam: "Royal Challengers Bangalore",
    nMatches: 7,
    nWon: 4,
    nLoss: 3,
    NRR: 0.597,
    oFor: {
      nRuns: 1217,
      nOvers: 140,
    },
    oAgainst: {
      nRuns: 1066,
      nOvers: 131.5,
    },
    nPoints: 8,
  },
  {
    nRank: 3,
    sTeam: "Delhi Capitals",
    nMatches: 7,
    nWon: 4,
    nLoss: 3,
    NRR: 0.319,
    oFor: {
      nRuns: 1085,
      nOvers: 126,
    },
    oAgainst: {
      nRuns: 1136,
      nOvers: 137,
    },
    nPoints: 8,
  },
  {
    nRank: 4,
    sTeam: "Rajasthan Royals",
    nMatches: 7,
    nWon: 3,
    nLoss: 4,
    NRR: 0.331,
    oFor: {
      nRuns: 1066,
      nOvers: 128.333,
    },
    oAgainst: {
      nRuns: 1094,
      nOvers: 137.167,
    },
    nPoints: 6,
  },
  {
    nRank: 5,
    sTeam: "Mumbai Indians",
    nMatches: 8,
    nWon: 2,
    nLoss: 6,
    NRR: -1.75,
    oFor: {
      nRuns: 1003,
      nOvers: 155.333,
    },
    oAgainst: {
      nRuns: 1134,
      nOvers: 138.167,
    },
    nPoints: 4,
  },
];

function truncateToDecimals(num, dec = 3) {
  const calcDec = Math.pow(10, dec);
  return Math.trunc(num * calcDec) / calcDec;
}

app.get("/", (req, res) => {
  // we are cloning the mainData into data without its reference, so that the mainData is not changed
  const data = JSON.parse(JSON.stringify(mainData));
  console.log("======== ORIGINAL DATA START ========");
  console.log(data);

  // checking if the required inputs are present
  if (!req.body.sYourTeam) return res.send("sYourTeam not found!");
  if (!req.body.sOppTeam) return res.send("sOppTeam not found!");
  if (!req.body.nTotalOvers) return res.send("nTotalOvers not found!");
  if (!req.body.nDesiredPos) return res.send("nDesiredPos not found!");

  // checking if sTossResult is present and restricting its value to either "Bowl First" or "Bat First"
  
  if (
    !req.body.sTossResult ||
    (req.body.sTossResult != "Bat First" &&
      req.body.sTossResult != "Bowl First")
  )
    return res.send("sTossResult not found!");
  if (!req.body.nRunsScored) return res.send("nRunsScored not found!");

  // get the index of current team and opposition team

  let currTeamIdx = data.findIndex((e) => e.sTeam === req.body.sYourTeam);

  let oppTeamIdx = data.findIndex((e) => e.sTeam === req.body.sOppTeam);

  if (currTeamIdx<0 || oppTeamIdx<0) return res.send("Invalid team names");

  // this is to check if the user wants to move to the same current rank.

  if (data[currTeamIdx].nRank == req.body.nDesiredPos)
    return res.status(200).json({
      message: "Already at the desired position!",
    });

  // this is used to handle the case where user wants to go bellow its current ranking.

  if(data[currTeamIdx].nRank < req.body.nDesiredPos)
    return res.status(200).json({
      message: "Umm, no not a good choice!",
    });

  // bat first and bowl first logics seperated using if else

  if (req.body.sTossResult === "Bat First") {
    // Bat First
    data[currTeamIdx].oFor.nRuns += req.body.nRunsScored;
    data[currTeamIdx].oFor.nOvers += req.body.nTotalOvers;
    
    data[oppTeamIdx].oAgainst.nRuns += req.body.nRunsScored;
    data[oppTeamIdx].oAgainst.nOvers += req.body.nTotalOvers;

    data[currTeamIdx].oAgainst.nOvers += req.body.nTotalOvers;

    data[oppTeamIdx].oFor.nOvers += req.body.nTotalOvers;

    data[currTeamIdx].nPoints += 2;

    if(data[req.body.nDesiredPos-1].nPoints > data[currTeamIdx].nPoints)
      return res.status(200).json({
        message: "Impossible desired position!"
      })

    let incRunCounter = 0;

    while(incRunCounter < req.body.nRunsScored && currTeamIdx + 1 != req.body.nDesiredPos)
    {
      incRunCounter++;
      data[currTeamIdx].oAgainst.nRuns += 1;
      data[oppTeamIdx].oFor.nRuns += 1;

      data[currTeamIdx].NRR = ((data[currTeamIdx].oFor.nRuns / data[currTeamIdx].oFor.nOvers) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers));
      data[oppTeamIdx].NRR = ((data[oppTeamIdx].oFor.nRuns / data[oppTeamIdx].oFor.nOvers) - (data[oppTeamIdx].oAgainst.nRuns / data[oppTeamIdx].oAgainst.nOvers));

      data.sort((a, b) => {if(a.nPoints === b.nPoints){ return b.NRR - a.NRR } return a.nPoints > b.nPoints ? -1 : 1; });
    
      currTeamIdx = data.findIndex((e) => e.sTeam === req.body.sYourTeam);
      oppTeamIdx = data.findIndex((e) => e.sTeam === req.body.sOppTeam);
    }

    console.log("======== DATA AFTER START RANGE ========");
    console.log(data);

    const incRunCounterStart = incRunCounter;

    const startNRR = truncateToDecimals(((data[currTeamIdx].oFor.nRuns / data[currTeamIdx].oFor.nOvers) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers)));


    while(incRunCounter < req.body.nRunsScored && currTeamIdx + 1 == req.body.nDesiredPos)
    {
      incRunCounter++;
      data[currTeamIdx].oAgainst.nRuns += 1;
      data[oppTeamIdx].oFor.nRuns += 1;

      data[currTeamIdx].NRR = ((data[currTeamIdx].oFor.nRuns / data[currTeamIdx].oFor.nOvers) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers));
      data[oppTeamIdx].NRR = ((data[oppTeamIdx].oFor.nRuns / data[oppTeamIdx].oFor.nOvers) - (data[oppTeamIdx].oAgainst.nRuns / data[oppTeamIdx].oAgainst.nOvers));

      data.sort((a, b) => {if(a.nPoints === b.nPoints){ return b.NRR - a.NRR } return a.nPoints > b.nPoints ? -1 : 1; });
    
      currTeamIdx = data.findIndex((e) => e.sTeam === req.body.sYourTeam);
      oppTeamIdx = data.findIndex((e) => e.sTeam === req.body.sOppTeam);
    }

    const incRunCounterEnd = incRunCounter-1;

    const endNRR = truncateToDecimals((((data[oppTeamIdx].oFor.nRuns-1) / data[oppTeamIdx].oFor.nOvers) - (data[oppTeamIdx].oAgainst.nRuns / data[oppTeamIdx].oAgainst.nOvers)));

    console.log("======== DATA AFTER END RANGE ========");
    console.log(data);

    return res.status(200).json({
        messageA: `If ${req.body.sYourTeam} score ${req.body.nRunsScored} runs in ${req.body.nTotalOvers} overs, ${req.body.sYourTeam} need to restrict ${req.body.sOppTeam} between ${incRunCounterStart} to ${incRunCounterEnd} runs in ${req.body.nTotalOvers}.`,
        messageB: `Revised NRR of ${req.body.sYourTeam} will be between ${endNRR} to ${startNRR}`
    })

  } else if (req.body.sTossResult === "Bowl First") {
    // Bowl First

    data[oppTeamIdx].oFor.nRuns += req.body.nRunsScored;
    data[oppTeamIdx].oFor.nOvers += req.body.nTotalOvers;

    data[oppTeamIdx].oAgainst.nRuns += req.body.nRunsScored+1;

    data[currTeamIdx].oAgainst.nRuns += req.body.nRunsScored;
    data[currTeamIdx].oAgainst.nOvers += req.body.nTotalOvers;

    data[currTeamIdx].oFor.nRuns += req.body.nRunsScored+1;

    data[currTeamIdx].nPoints += 2;

    if(data[req.body.nDesiredPos-1].nPoints > data[currTeamIdx].nPoints)
      return res.status(200).json({
        message: "Impossible desired position!"
      })

    let incOverCounter = 0;

    while(incOverCounter < req.body.nTotalOvers && currTeamIdx + 1 != req.body.nDesiredPos)
    {
        incOverCounter+=0.166666667;
        data[currTeamIdx].oFor.nOvers += 0.166666667;
        data[oppTeamIdx].oAgainst.nOvers += 0.166666667;
  
        data[currTeamIdx].NRR = ((data[currTeamIdx].oFor.nRuns / data[currTeamIdx].oFor.nOvers) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers));
        data[oppTeamIdx].NRR = ((data[oppTeamIdx].oFor.nRuns / data[oppTeamIdx].oFor.nOvers) - (data[oppTeamIdx].oAgainst.nRuns / data[oppTeamIdx].oAgainst.nOvers));
  
        data.sort((a, b) => {if(a.nPoints === b.nPoints){ return b.NRR - a.NRR } return a.nPoints > b.nPoints ? -1 : 1; });
      
        currTeamIdx = data.findIndex((e) => e.sTeam === req.body.sYourTeam);
        oppTeamIdx = data.findIndex((e) => e.sTeam === req.body.sOppTeam);
    }

    console.log("======== DATA AFTER START RANGE ========");
    console.log(data);

    const startNRR = truncateToDecimals(((data[currTeamIdx].oFor.nRuns / data[currTeamIdx].oFor.nOvers) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers)));

    const incStartOverCounter = truncateToDecimals(incOverCounter);

    while(incOverCounter < req.body.nTotalOvers && currTeamIdx + 1 == req.body.nDesiredPos)
    {
        incOverCounter+=0.166666667;
        data[currTeamIdx].oFor.nOvers += 0.166666667;
        data[oppTeamIdx].oAgainst.nOvers += 0.166666667;
  
        data[currTeamIdx].NRR = ((data[currTeamIdx].oFor.nRuns / data[currTeamIdx].oFor.nOvers) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers));
        data[oppTeamIdx].NRR = ((data[oppTeamIdx].oFor.nRuns / data[oppTeamIdx].oFor.nOvers) - (data[oppTeamIdx].oAgainst.nRuns / data[oppTeamIdx].oAgainst.nOvers));
  
        data.sort((a, b) => {if(a.nPoints === b.nPoints){ return b.NRR - a.NRR } return a.nPoints > b.nPoints ? -1 : 1; });
      
        currTeamIdx = data.findIndex((e) => e.sTeam === req.body.sYourTeam);
        oppTeamIdx = data.findIndex((e) => e.sTeam === req.body.sOppTeam);
    }

    const endNRR = truncateToDecimals(((data[currTeamIdx].oFor.nRuns / (data[currTeamIdx].oFor.nOvers-0.166666667)) - (data[currTeamIdx].oAgainst.nRuns / data[currTeamIdx].oAgainst.nOvers)));

    let incEndOverCounter;

    if(currTeamIdx + 1 == req.body.nDesiredPos)
      incEndOverCounter = truncateToDecimals(incOverCounter);
    else
      incEndOverCounter = truncateToDecimals(incOverCounter-0.166666667);

    console.log("======== DATA AFTER END RANGE ========");
    console.log(data);

    return res.status(200).json({
      messageA: `${req.body.sYourTeam} need to chase ${Number(req.body.nRunsScored)} between ${incStartOverCounter} and ${incEndOverCounter} Overs.`,
      messageB: `Revised NRR of ${req.body.sYourTeam} will be between ${endNRR} to ${startNRR}`
    })

  }
});

const server = app.listen(2000, () => {
  console.log("listening!");
});
