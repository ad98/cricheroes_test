Hi, please follow the bellow steps to work with this project.

step1 : clone the project.
step2 : npm i (for installing project dependencies)
step3 : node index.js

api information: 

- there is a single api which does it all.
- "http://localhost:2000/" call this api, by passing appropriate body data to it.

ex body data:

{
    "sYourTeam": "Rajasthan Royals",
    "sOppTeam": "Royal Challengers Bangalore",
    "nTotalOvers": 20,
    "nDesiredPos": 3,
    "sTossResult": "Bat First",
    "nRunsScored": 80
}
